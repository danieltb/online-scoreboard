var mysql = require('mysql')
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'readwrite',
  password: 'mysecretpwd',
  multipleStatements: true
});

connection.connect(function(err) {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }

  console.log('connected as id ' + connection.threadId);
  connection.query("use scoreboard", (error, results, fields) => !error ? console.log("using scoreboard database\n") : console.log("there was an error setting the scoreboard"))  
});

module.exports = connection;
use scoreboard;

drop table games;
CREATE TABLE games
(
    gameId              INT unsigned NOT NULL AUTO_INCREMENT,
    home            VARCHAR(150) NOT NULL,
    away            VARCHAR(150) NOT NULL,
    gameDay          DATETIME NOT NULL DEFAULT NOW(),  
    PRIMARY KEY     (gameId) 
);


drop table scores;
CREATE TABLE scores
(
    scoreId         INT unsigned NOT NULL AUTO_INCREMENT,
    gameId          INT unsigned NOT NULL,
    time            DATETIME NOT NULL DEFAULT NOW(),
    homeBehinds      INT unsigned NOT NULL DEFAULT 0,
    homeGoals       INT unsigned NOT NULL DEFAULT 0,
    awayBehinds      INT unsigned NOT NULL DEFAULT 0,
    awayGoals       INT unsigned NOT NULL DEFAULT 0,
    PRIMARY KEY (scoreId),
    FOREIGN KEY (gameId) REFERENCES games(gameID)
);

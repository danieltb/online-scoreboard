use scoreboard;
CREATE TRIGGER new_game_trig
AFTER INSERT ON `games` FOR EACH ROW
begin 
    INSERT INTO scores (gameId) 
    VALUES (NEW.gameId);
END;
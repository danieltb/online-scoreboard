CREATE USER 'readwrite'@'localhost' IDENTIFIED BY 'mysecretpwd';

GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP
    ON scoreboard.*
    TO 'readwrite'@'localhost';
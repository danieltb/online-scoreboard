use scoreboard;

-- INSERT INTO games VALUES
--     (0, 'Colts', 'Surrey Park', '2000-01-01 12:00:01');

INSERT INTO scores (gameId, homeBehinds, homeGoals, awayBehinds, awayGoals) VALUES
    (1, 1, 2, 3, 4);
INSERT INTO scores (gameId, homeBehinds, homeGoals, awayBehinds, awayGoals) VALUES
    (1, 2, 3, 4, 6);

use scoreboard;
select * from scores;
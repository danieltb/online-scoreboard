const express = require("express");
const bodyParser = require("body-parser");
const fs = require("fs");
const mustache = require("mustache");
const db = require("./sqlconnection.js");
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}))

//load files
const loadFile = (filePath, callback) => {
    const file = fs.readFile(filePath, "UTF-8", callback())
}
loadFile("./html/scoreboardHtmlTemplate.html",
    () => (err, scoreBoardData) => loadFile("./html/adminHtmlTemplate.html",
        () => (error, adminData) => loadFile("./html/homeHtmlTemplate.html",
            () => (error, homeData) => runServer(scoreBoardData, adminData, homeData)
        )
    )
);

const runServer = (scoreBoardData, adminData, homeData) => {
    const portNum = 8888;
    app.use((req, res, next) => {
        console.log("\nReceived a request");
        console.log(req.url);
        console.log(req.params);
        console.log(req.body);
        next();
    })
    app.get("/", (req,res) => res.send(homeData))
    app.post("/", (req, res) => db.query(putNewGame, [req.body.home, req.body.away], reDirect(req,res)))

    app.get("/game/:gameId", (req, res) => db.query(getLatestScores, req.params.gameId, sendRenderedResults(req, res, scoreboarddata)));
    
    // get admin page
    app.get("/game/:gameId/admin", (req, res) => db.query(getLatestScores, req.params.gameId, sendRenderedResults(req, res, admindata)));
    // update scores in db by looking at post body
    app.post("/game/:gameId/admin", (req, res) => {
        console.log("received request:");
        console.log(req.body);
        console.log([req.params.gameId, req.body.homeBehinds, req.body.homeGoals, req.body.awayBehinds, req.body.awayGoals].map(a => a + ""));
        db.query(putLatestScores, [req.params.gameId, req.body.homeBehinds, req.body.homeGoals, req.body.awayBehinds, req.body.awayGoals]
            , handleQuery);
        res.status(200);
        res.redirect("/game/" + req.params.gameId + "/admin");
    });
    app.listen(portNum);
    console.log("listening on port " + portNum);
}

//QUERIES
const putNewGame = "INSERT INTO games (home, away) VALUES (?, ?); SELECT LAST_INSERT_ID();";
const getLatestScores = "SELECT * from scores WHERE gameId = ? ORDER BY time DESC  LIMIT 1;";
const putLatestScores = "INSERT INTO scores (gameId, homeBehinds, homeGoals, awayBehinds, awayGoals) VALUES ( ?, ?, ?, ?, ?);";


//CALLBACKS
const sendRenderedResults = (req, res, data) => (error, results, fields) => {
    handleQuery()
    
    results = results[0];
    results.homeTotal = results.homeBehinds + results.homeGoals * 6
    results.awayTotal = results.awayBehinds + results.awayGoals * 6
    console.log("Query responded with results: ");
    console.log(results);
    res.send(mustache.render(data, results));
}
const handleQuery = (error, results, fields) => {
    if (error) { console.log("there was an error handlingQuery");console.log(error); res.redirect("/") }
    console.log(results);
    return;
};
const reDirect = (req, res) => (error, results, fields) => {
    handleQuery(error, results, fields);
    const gameId = results[1][0]["LAST_INSERT_ID()"]
    res.redirect("/game/" + gameId);
};